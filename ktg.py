import numpy as np
import scipy as sp
import math
import matplotlib.pyplot as plt
import sys
import scipy.special as special
from scipy.integrate import quad
from basics import Definitions
from elements import *

spectrum = Spectrum() # Call the class to use one of its functions
eloss = Eloss()

class kTDyG_noEloss(Sudakov): # ktg distribution without energy loss
    def __init__(self):
        super().__init__()

    def vacuum_int(self,theta,kt,a,fl='q'): # vacuum splittings only
        z = kt/theta
        kappa = z*pow(theta,a)
        jacobian = 1/theta # coming from using delta(z*theta-kt)
        return self.Pvac(z,theta,fl)*self.DeltaVac(kappa,a,fl)*jacobian
    
    def vacuum(self,kt,a,fl='q'): # vacuum splittings only
        thetamin = kt # coming from using delta(z*theta-kt) and noting that thetmax=R
        thetamax = self.R_jet
        return quad(self.vacuum_int,thetamin,thetamax,args=(kt,a,fl))[0]

    def vacuum_analytic(self,kt,a,fl='q'):
        if fl=='q':
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca
        abar = self.alphas_ref(self.pt_jet*self.R_jet)*Ci/math.pi
        return 2*abar*np.exp(-abar*pow(np.log(kt/test.R_jet),2))*np.log(test.R_jet/kt)/kt

    def vacuum_int_veto(self,theta,kt,a,fl='q'):
        z = kt/theta
        kappa = z*pow(theta,a)
        jacobian = 1/theta
        return self.Pvac(z,theta,fl)*self.DeltaVac_veto(kappa,a,fl)*jacobian

    def vacuum_veto(self,kt,a,fl='q'): # vacuum like emissions including the veto
        thetamin = kt
        thetamax = self.R_jet
        #Area_full: z*(theta/R)^a > kappa
        res_full = quad(self.vacuum_int_veto,thetamin,thetamax,args=(kt,a,fl))[0]
        #Area veto: td < tf < L
        ktmin = 2/(self.pt_jet*self.xi*self.R_jet)
        ktmax = min(self.R_jet, pow(2*self.qhat0/pow(self.pt_jet,3),1./4),pow(2*self.qhat0/(self.theta_c*self.pt_jet),1./3),np.sqrt(self.qhat0*self.xi)/self.pt_jet)
        thetamin = max(kt, self.theta_c, 2/(self.pt_jet*self.xi*kt))
        thetamax = min(self.R_jet, 2*self.qhat0/pow(kt*self.pt_jet,3))
        res_veto = 0
        if ktmin<=kt<=ktmax and thetamin<thetamax: 
            res_veto = quad(self.vacuum_int_veto,thetamin,thetamax,args=(kt,a,fl))[0]
        return res_full - res_veto

    def mie_int(self,theta,kt,a,fl='q'):
        z = kt/theta
        kappa = z*pow(theta,a)
        jacobian = 1/theta
        return self.Pmed(z,fl)*self.Pbroad(z,theta)*self.DeltaMed(kappa,a,fl)*jacobian

    def mie(self,kt,a,fl='q'): # medium induced emissions only
        thetamin = max(self.pt_jet * kt / self.wc, kt)
        thetamax = self.R_jet
        return quad(self.mie_int,thetamin,thetamax,args=(kt,a,fl))[0]/(1.-self.DeltaMed(1e-8,a,fl))

    def medium_int_veto(self,theta,kt,a,fl='q'):
        z = kt/theta
        kappa = z*pow(theta,a)
        jacobian = 1/theta
        #Area veto: td < tf < L
        ktmin = 2/(self.pt_jet*self.xi*self.R_jet)
        ktmax = min(self.R_jet, pow(2*self.qhat0/pow(self.pt_jet,3),1./4),pow(2*self.qhat0/(self.theta_c*self.pt_jet),1./3),np.sqrt(self.qhat0*self.xi)/self.pt_jet)
        thetamin = max(kt, self.theta_c, 2/(self.pt_jet*self.xi*kt))
        thetamax = min(self.R_jet, 2*self.qhat0/pow(kt*self.pt_jet,3))
        if thetamin<=theta<=thetamax and ktmin<=kt<=ktmax: # an alternative way of implementing the veto before integration
            pvac = 0.0
        else:
            pvac = self.Pvac(z,theta,fl)
        return (pvac+self.Pmed(z,fl)*self.Pbroad(z,theta)*self.step(self.wc-z*self.pt_jet))*self.DeltaMed(kappa,a,fl)*self.DeltaVac_veto(kappa,a,fl)*jacobian

    def medium_veto(self,kt,a,fl='q'):
        thetamin = kt
        thetamax = self.R_jet
        return quad(self.medium_int_veto,thetamin,thetamax,args=(kt,a,fl))[0]

    def ktg_matched(self,kt,a,mode='all'):
        quark_xs = spectrum.fit(self.pt_jet,self.rap_jet,'q','p')[0]
        gluon_xs = spectrum.fit(self.pt_jet,self.rap_jet,'g','p')[0]
        total_xs = quark_xs + gluon_xs
        if mode=='q': # only quarks
            return quark_xs*self.vacuum(kt,a,'q') # this is for the q/g fraction studies
        elif mode=='g': # only gluons
            return gluon_xs*self.vacuum(kt,a,'g')
        else: # quarks and gluons
            return (quark_xs*self.vacuum(kt,a,'q')+self.vacuum(kt,a,'g')*gluon_xs)/total_xs

    def normalisation(self,a,fl='all'):
        ktmin = 0
        ktmax = self.R_jet
        return quad(lambda kt: self.ktg_matched(kt,a,fl),ktmin,ktmax)[0]
    
class kTDyG_wEloss(kTDyG_noEloss): # ktg distribution with energy loss and q/g fraction
    def __init__(self):
        super().__init__()
    
    def ktg_matched_qw_int_eloss(self,theta,kt,a,mode='all'):
        eloss_q = eloss.qw_eloss_rough(theta,'q')
        eloss_g = eloss.qw_eloss_rough(theta,'g')
        quark_xs = spectrum.fit(self.pt_jet,self.rap_jet,'q','Pb')[0]
        gluon_xs = spectrum.fit(self.pt_jet,self.rap_jet,'g','Pb')[0]
        if mode=='q': # only quarks
            return eloss_q*quark_xs*self.medium_int_veto(theta,kt,a,'q') # this is for the q/g fraction studies
        elif mode=='g': # only gluons
            return eloss_g*gluon_xs*self.medium_int_veto(theta,kt,a,'g')
        else: # quarks and gluons
            return eloss_q*quark_xs*self.medium_int_veto(theta,kt,a,'q')+eloss_g*gluon_xs*self.medium_int_veto(theta,kt,a,'g')
    
    def ktg_matched_qw_eloss(self,kt,a,mode='all'):
        thetamin = kt
        thetamax = self.R_jet
        return quad(self.ktg_matched_qw_int_eloss,thetamin,thetamax,args=(kt,a,mode))[0]

    def ktg_felix_int(self,theta,kt,a,mode='all'):
        eloss_q = eloss.qw_eloss_coherent('q')
        eloss_g = eloss.qw_eloss_coherent('g')
        quark_xs = spectrum.fit(self.pt_jet,self.rap_jet,'q','Pb')[0]
        gluon_xs = spectrum.fit(self.pt_jet,self.rap_jet,'g','Pb')[0]
        if mode=='q': # only quarks
            return eloss_q*quark_xs*self.vacuum_int(theta,kt,a,'q') # this is for the q/g fraction studies
        elif mode=='g': # only gluons
            return eloss_g*gluon_xs*self.vacuum_int(theta,kt,a,'g')
        else: # quarks and gluons
            return eloss_q*quark_xs*self.vacuum_int(theta,kt,a,'q')+eloss_g*gluon_xs*self.vacuum_int(theta,kt,a,'g')

    def ktg_felix(self,kt,a,mode='all'):
        thetamin = kt
        thetamax = self.R_jet
        return quad(self.ktg_felix_int,thetamin,thetamax,args=(kt,a,mode))[0]

    def raa(self,a): 
        ktmin = 0.
        ktmax = self.R_jet
        numerator= quad(lambda kt: self.ktg_matched_qw_eloss(kt,a,'all'),ktmin,ktmax,epsrel=1e-4)[0]
        #quark_xs_medium = spectrum.fit(self.pt_jet,self.rap_jet,'q','Pb')[0]
        #gluon_xs_medium= spectrum.fit(self.pt_jet,self.rap_jet,'g','Pb')[0]
       # numerator = quark_xs_medium + gluon_xs_medium
        quark_xs_vacuum = spectrum.fit(self.pt_jet,self.rap_jet,'q','p')[0]
        gluon_xs_vacuum = spectrum.fit(self.pt_jet,self.rap_jet,'g','p')[0]
        return numerator/(quark_xs_vacuum+gluon_xs_vacuum) # the denominator corresponds to the normalisation of the 
                                                           # kt_g distribution in vacuum

    def normalisation(self,a,fl='q'):
        ktmin = 0
        ktmax = self.R_jet
        return quad(lambda kt: self.ktg_matched_qw_eloss(kt,a,fl),ktmin,ktmax)[0]
