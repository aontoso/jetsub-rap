#! /bin/bash
#! -S bin/bash
#$ -cwd

Rjet=0.4 
Lav=4
ptjets=(120.0 30.0)
avalue=1
flav='q'
# Generate the files that we are going to need to calculate ktg_g distribution
#python3 generate-ktg.py $Rjet
# Read off the ktg values from a .dat file
ktg=($(cat ktg_values.dat)) 
for i in $(seq ${#ktg[*]}); do
    [[ ${ktg[$i]} = $name ]] && echo "${ktg[$i]}"
done
# Read off the rapidity values from a .dat file
yjets=($(cat yvalues.dat)) 
for i in $(seq ${#yjets[*]}); do
    [[ ${yjets[$i]} = $name ]] && echo "${yjets[$i]}"
done
# Run the loops
#for flav in "${flavs[@]}"
#do
#    echo ${flav}
for yjet in "${yjets[@]}"
do  
    echo ${yjet}
    for ptjet in "${ptjets[@]}"
    do
    #    echo ${ptjet}
     #   for ktgval in "${ktg[@]}"
     #   do
            #echo ${ktgval}
        python3 run-ktg.py $Rjet ${ptjet} ${yjet} $Lav $avalue 
       # python3 run-ktg.py $Rjet ${ptjet} ${yjet} $Lav $avalue ${flav}
       # done
      #  done
    done
done
# Merge the files
#for flav in "${flavs[@]}"
#do
#for ptjet in "${ptjets[@]}"
#    do
#    for yjet in "${yjets[@]}"
#    do
  #  for ktgval in "${ktg[@]}"
   #     do  
 #       cat paper_results/med_R${Rjet}_pt${ptjet}_yjet${yjet}_ktg* >> paper_results/med_R${Rjet}_pt${ptjet}_yjet${yjet}.dat
 #      rm paper_results/med_R${Rjet}_pt${ptjet}_yjet${yjet}_ktg*
    #done
 #   done
#done
