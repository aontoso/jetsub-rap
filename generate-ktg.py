import sys
import numpy as np

ktg_min = np.log10(0.00001)
ktg_max = np.log10(float(sys.argv[1]))
n_points = 500
logktg_values = np.zeros(n_points)

for i in range(0, n_points):
            ktg = pow(10,ktg_min + np.double(i)*(ktg_max - ktg_min)/np.double(n_points))
            logktg_values[i] = ktg;

data = np.array([logktg_values],'f')
data = data.T 
np.savetxt('ktg_values.dat',data,fmt='%.8f')

