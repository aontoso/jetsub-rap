import numpy as np
from scipy.integrate import quad
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import math
from scipy import special
from matplotlib import gridspec
from basics import Definitions
from elements import *
from ktg import *

prueba = Definitions()
font = {'color':  'black',
    'weight': 'normal',
    'size': 18
    }
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Palatino']
plt.rcParams['xtick.labelsize'] = 10
plt.rcParams['ytick.labelsize'] = 10
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['legend.handlelength'] = 1

# Grab all the stuff we need
ktgvalues = np.loadtxt('ktg_values.dat', delimiter=' ', unpack=True)
rapvalues = np.loadtxt('yvalues.dat', delimiter=' ', unpack=True)
dsigmadktg_pt120y0 = np.loadtxt('paper_results/vac_R0.4_pt120.0_yjet0.15.dat', delimiter=' ', unpack=True) 
dsigmadktg_pt30y0 = np.loadtxt('paper_results/vac_R0.4_pt30.0_yjet0.15.dat', delimiter=' ', unpack=True)

with PdfPages('ktg-vac-30GeV.pdf') as pdf:
  #----------------------------------------------------------------
  fig = plt.figure(figsize=(5,3.8))
  gs = gridspec.GridSpec(2, 1, height_ratios=[3,1])
  ax = plt.gca()

  # generic setup
  ax0 = plt.subplot(gs[0])
  ax0.grid(True, lw=0.5, ls=':', zorder=0)
  ax0.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  ax0.set_ylim(0,50)
  ax0.set_xscale('log')
  ax0.set_xlim(0.001,1)
  ax0.set_ylabel(r'$\frac{1}{\sigma}\frac{d\sigma}{dk_{t}}$',fontsize=12)
  n = np.size(rapvalues)-1
  colors = plt.cm.turbo(np.linspace(0,1,n))

  ax1 = plt.subplot(gs[1], sharex = ax0)
  ax1.set_xscale('log')
  plt.setp(ax0.get_xticklabels(), visible=False)
  ax1.set_ylabel(r'Ratio to $y=0$',fontsize=11)
  ax1.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
  ax1.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  ax1.set_ylim([0.5,2.7])
  tks=[0.7,1.1,1.5,1.9,2.3]
  ax1.set_yticks(tks)

  for i in range(0, n):
      icolor = i;
      y = rapvalues[i]
      dsigmadktg = np.loadtxt('paper_results/vac_R0.4_pt30.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True) 
      ax0.plot(ktgvalues/prueba.R_jet, dsigmadktg,color=colors[icolor])
      ax1.plot(ktgvalues/prueba.R_jet, dsigmadktg/dsigmadktg_pt30y0,color=colors[icolor])

      caca=ax0.scatter(ktgvalues/prueba.R_jet, dsigmadktg,c=np.linspace(min(rapvalues),rapvalues[n-1],np.size(ktgvalues)),s=0,marker='o', cmap='turbo')


  ax1.plot([ax1.get_xlim()[0],ax1.get_xlim()[1]],[1,1], ls='--', lw=1, color='gray')
    
  ax0.text(0.3, 0.94, r'Vacuum(DLA)', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='black')
  ax0.text(0.3, 0.88, r'$p_t=30$ GeV, $R=0.4$', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax1.set_xlabel(r'$k_t=z\theta/R$',fontsize=12)

  axlist = [ax0,ax1] 
  plt.subplots_adjust(hspace=.0,wspace=.0)
  cbar=plt.colorbar(caca,ax=axlist)
  ax1.text(0.97, 0.5, r'$y_{\rm jet}$', ha='left', va='baseline', 
          bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=12, rotation=270) 
  pdf.savefig(bbox_inches='tight')
  plt.close()