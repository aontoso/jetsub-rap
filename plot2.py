import numpy as np
from scipy.integrate import quad
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import math
from scipy import special
from matplotlib import gridspec
from basics import Definitions
from elements import *
from ktg import *
from scipy import integrate
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

prueba = Definitions()
font = {'color':  'black',
    'weight': 'normal',
    'size': 18
    }
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Palatino']
plt.rcParams['xtick.labelsize'] = 10
plt.rcParams['ytick.labelsize'] = 10
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['legend.handlelength'] = 1

# Grab all the stuff we need
ktgvalues = np.loadtxt('ktg_values.dat', delimiter=' ', unpack=True)
rapvalues = np.loadtxt('yvalues.dat', delimiter=' ', unpack=True)

with PdfPages('ktg-med-qg-30GeV.pdf') as pdf:
  #----------------------------------------------------------------
  fig = plt.figure(figsize=(5,3.8))
  gs = gridspec.GridSpec(2, 1, height_ratios=[3,1])
  ax = plt.gca()

  # generic setup
  ax0 = plt.subplot(gs[0])
  ax0.grid(True, lw=0.5, ls=':', zorder=0)
  ax0.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  ax0.set_ylim(0,100)
  ax0.set_xscale('log')
  ax0.set_xlim(0.001,1)
  ax0.set_ylabel(r'$\frac{1}{\sigma}\frac{d\sigma}{dk_{t}}$',fontsize=12)
  n = np.size(rapvalues)-1
  colors = plt.cm.turbo(np.linspace(0,1,n))

  ax1 = plt.subplot(gs[1], sharex = ax0)
  ax1.set_xscale('log')
  plt.setp(ax0.get_xticklabels(), visible=False)
  ax1.set_ylabel(r'Ratio to $pp$',fontsize=11)
  ax1.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
  ax1.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  #ax1.set_ylim([0.9,1.2])
  #tks=[0.95,1,1.05,1.1,1.15]
  #ax1.set_yticks(tks)

  for i in range(0, n):
      icolor = i;
      y = rapvalues[i]
      dsigmadktg = np.loadtxt('paper_results/vac_R0.4_pt30.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True) 
      dsigmadktg_mie = np.loadtxt('paper_results/felix_R0.4_pt30.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
      norm = integrate.simps(dsigmadktg_mie, ktgvalues)
      ax0.plot(ktgvalues/prueba.R_jet, dsigmadktg_mie/norm,color=colors[icolor])
      ax1.plot(ktgvalues/prueba.R_jet, dsigmadktg_mie/(dsigmadktg*norm),color=colors[icolor])
      caca=ax0.scatter(ktgvalues/prueba.R_jet, dsigmadktg,c=np.linspace(min(rapvalues),rapvalues[n-1],np.size(ktgvalues)),s=0,marker='o', cmap='turbo')
  
  ax1.plot([ax1.get_xlim()[0],ax1.get_xlim()[1]],[1,1], ls='--', lw=1, color='gray')
    
  ax0.text(0.41, 0.82, r'$\hat q=1.5$ GeV/fm', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.41, 0.76, r'$L=4$ fm, $\alpha_s^{\rm med}=0.24$', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.41, 0.88, r'$p_t=30$ GeV,$R=0.4$', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.41, 0.94, r'$q/g$ frac. model', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='black')

  ax1.set_xlabel(r'$k_t=z\theta/R$',fontsize=12)

  axlist = [ax0,ax1] 
  plt.subplots_adjust(hspace=.0,wspace=.0)
  cbar=plt.colorbar(caca,ax=axlist)
  ax1.text(0.97, 0.5, r'$y_{\rm jet}$', ha='left', va='baseline', 
          bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=12, rotation=270) 
  pdf.savefig(bbox_inches='tight')
  plt.minorticks_on()
  plt.close()