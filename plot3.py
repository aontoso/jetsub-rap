import numpy as np
from scipy.integrate import quad
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import math
from scipy import special
from matplotlib import gridspec
from basics import Definitions
from scipy import integrate
from elements import *
from ktg import *
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

prueba = Definitions()
font = {'color':  'black',
    'weight': 'normal',
    'size': 18
    }
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Palatino']
plt.rcParams['xtick.labelsize'] = 10
plt.rcParams['ytick.labelsize'] = 10
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['legend.handlelength'] = 1

# Grab all the stuff we need
ktgvalues = np.loadtxt('ktg_values.dat', delimiter=' ', unpack=True)
rapvalues = np.loadtxt('yvalues.dat', delimiter=' ', unpack=True)

with PdfPages('ktg-med-120GeV.pdf') as pdf:
  #----------------------------------------------------------------
  fig = plt.figure(figsize=(5,3.8))
  gs = gridspec.GridSpec(2, 1, height_ratios=[3,1])
  ax = plt.gca()

  # generic setup
  ax0 = plt.subplot(gs[0])
  ax0.grid(True, lw=0.5, ls=':', zorder=0)
  ax0.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  ax0.set_ylim(0,100)
  ax0.set_xscale('log')
  ax0.set_xlim(0.001,1)
  ax0.set_ylabel(r'$\frac{1}{\sigma}\frac{d\sigma}{dk_{t}}$',fontsize=12)
  n = np.size(rapvalues)-1
  colors = plt.cm.turbo(np.linspace(0,1,n))

  ax1 = plt.subplot(gs[1], sharex = ax0)
  plt.setp(ax0.get_xticklabels(), visible=False)

  ax1.set_ylabel(r'Ratio to $pp$',fontsize=11)
  ax1.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
  ax1.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  ax1.set_ylim([0.4,1.8])
  tks=[0.6,0.8,1.,1.2,1.4,1.6]
  ax1.set_yticks(tks)

  for i in range(0, n):
      icolor = i;
      y = rapvalues[i]
      dsigmadktg = np.loadtxt('paper_results/vac_R0.4_pt120.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True) 
      dsigmadktg_mie = np.loadtxt('paper_results/med_R0.4_pt120.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
      norm = integrate.simps(dsigmadktg_mie, ktgvalues)
      ax0.plot(ktgvalues/prueba.R_jet, dsigmadktg_mie/norm,color=colors[icolor])
      ax1.plot(ktgvalues/prueba.R_jet, dsigmadktg_mie/(norm*dsigmadktg),color=colors[icolor])

      caca=ax0.scatter(ktgvalues/prueba.R_jet, dsigmadktg,c=np.linspace(min(rapvalues),rapvalues[n-1],np.size(ktgvalues)),s=0,marker='o', cmap='turbo')

  kt_Qs = prueba.Qs/120
  kt_thetac = prueba.theta_c/(2*0.4) # kt = z*theta/R = 0.5*thetac/R
  ax0.plot([kt_Qs,kt_Qs],[ax0.get_ylim()[0],ax0.get_ylim()[1]], ls='--', lw=1, color='gray')
  ax0.plot([kt_thetac,kt_thetac],[ax0.get_ylim()[0],ax0.get_ylim()[1]], ls='--', lw=1, color='gray')
  ax1.plot([ax1.get_xlim()[0],ax1.get_xlim()[1]],[1,1], ls='--', lw=1, color='gray')
    
  ax0.text(0.02, 0.88, r'$\hat q = 1.5$ GeV/fm', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.02, 0.82, r'$p_t=120$ GeV', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.02, 0.76, r'$L=4$ fm,$R=0.4$', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.02, 0.7, r'$\alpha_s^{\rm med}=0.24$', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.02, 0.94, r'$\theta_c$-model', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='black')
  ax0.text(0.29,0.55,r'$k_t=\frac{Q_s}{p_t}$', ha='left', va='baseline', 
          bbox=dict(facecolor='none',edgecolor='none'), transform=ax.transAxes, rotation=90,fontsize=10)
  ax0.text(0.46,0.65,r'$k_t=\frac{\theta_c}{2R}$', ha='left', va='baseline', 
          bbox=dict(facecolor='none',edgecolor='none'), transform=ax.transAxes, rotation=90,fontsize=10)



  ax1.set_xlabel(r'$k_t=z\theta/R$',fontsize=12)
  ax1.plot([kt_Qs,kt_Qs],[ax1.get_ylim()[0],ax1.get_ylim()[1]], ls='--', lw=1, color='gray')
  ax1.plot([kt_thetac,kt_thetac],[ax1.get_ylim()[0],ax1.get_ylim()[1]], ls='--', lw=1, color='gray')
  
  axlist = [ax0,ax1] 
  plt.subplots_adjust(hspace=.0,wspace=.0)
  cbar=plt.colorbar(caca,ax=axlist)
  ax1.text(0.97, 0.5, r'$y_{\rm jet}$', ha='left', va='baseline', 
          bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=12, rotation=270) 
  pdf.savefig(bbox_inches='tight')
  plt.minorticks_on()
  plt.close()