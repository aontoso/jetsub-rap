import numpy as np
import scipy as sp
import math
import matplotlib.pyplot as plt
import sys
import scipy.special as special
from scipy.integrate import quad
from basics import Definitions
from elements import *

spectrum = Spectrum() # Call the class to use one of its functions
eloss = Eloss()

class ThetaSD_noEloss(Sudakov_SD): # theta_g distribution as defined by SoftDrop without eloss
    def __init__(self):
        super().__init__()

    def vacuum(self,theta,zcut,fl='q'): # vacuum splittings only
        if fl=='q':  
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca
        alpha_bar = 2*self.alphas_vac*Ci/math.pi
        return alpha_bar*self.DeltaVac(theta,zcut,fl)*np.log(1/zcut)/theta

    def vacuum_veto(self,theta,zcut,fl='q'): # vacuum splittings with the veto
        if fl=='q':  
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca
        alpha_bar = 2*self.alphas_vac*Ci/math.pi
        #Area_full: z>zcut
        res_full = alpha_bar*self.DeltaVac_veto(theta,zcut,fl)*np.log(1/zcut)/theta
        #Area veto: td < tf < L
        thetamin = max(np.sqrt(2./(self.pt_jet*self.xi)),self.theta_c)
        thetamax = min(self.R_jet, pow(2*self.qhat0/pow(zcut*self.pt_jet,3),1/4))
        zmin = max(zcut, 2./(pow(theta,2)*self.pt_jet*self.xi))    
        zmax = min(1.0, pow(2.*self.qhat0/(pow(self.pt_jet,3)*pow(theta,4)),1./3.))
        res_veto = 0
        if thetamin<theta<thetamax and zmin<zmax: 
            res_veto = alpha_bar*self.DeltaVac_veto(theta,zcut,fl)*np.log(zmax/zmin)/theta
        return res_full - res_veto

    def mie_int(self,z,theta,zcut,fl='q'):
        return self.Pmed(z,fl)*self.Pbroad(z,theta)*self.DeltaMed(theta,zcut,fl)

    def mie(self,theta,zcut,fl='q'): # medium induced splittings only
        zmin = zcut
        zmax = self.wc/self.pt_jet
        return quad(self.mie_int,zmin,zmax,args=(theta,zcut,fl),epsrel=1e-8)[0]/(1.-self.DeltaMed(1e-8,zcut,fl))

    def medium_int_veto(self,z,theta,zcut,fl='q'): # heuristic combination of VLEs and MIEs
        #Area veto: td < tf < L
        thetamin = max(np.sqrt(2./(self.pt_jet*self.xi)), self.theta_c, np.sqrt(4/(pow(self.xi,3)*self.qhat0)))
        thetamax = min(self.R_jet,pow(2.*self.qhat0/pow(kappa*pow(self.R_jet,a)*self.pt_jet,3),1./(4.-3.*a)) if a<=4./3. else self.R_jet)
        zmin = max(zcut, 2./(pow(theta,2)*self.pt_jet*self.xi))    
        zmax = min(1.0, pow(2.*self.qhat0/(pow(self.pt_jet,3)*pow(theta,4)),1./3.))
        if thetamin<=theta<=thetamax and zmin<=z<=zmax: # an alternative way of implementing the veto.
            pvac = 0.0
        else:
            pvac = self.Pvac(z,theta,fl)
        return (pvac+self.Pmed(z,fl)*self.Pbroad(z,theta)*self.step(self.wc-z*self.pt_jet))*self.DeltaMed(theta,zcut,fl)*self.DeltaVac_veto(theta,zcut,fl)

    def medium_veto(self,theta,zcut,fl='q'):
        res_full = quad(self.medium_int_veto,zcut,1,args=(theta,zcut,fl),epsrel=1e-6)[0]
        return res_full 

    def thetag_matched(self,theta,zcut,mode='all'):
        quark_xs = spectrum.fit(self.pt_jet,self.rap_jet,'q','p')[0]
        gluon_xs = spectrum.fit(self.pt_jet,self.rap_jet,'g','p')[0]
        total_xs = quark_xs + gluon_xs
        if mode=='q': # only quarks
            return quark_xs*self.vacuum(theta,zcut,'q') # this is for the q/g fraction studies
        elif mode=='g': # only gluons
            return gluon_xs*self.vacuum(theta,zcut,'g')
        else: # quarks and gluons
            return (quark_xs*self.vacuum(theta,zcut,'q')+self.vacuum(theta,zcut,'g')*gluon_xs)/total_xs

class ThetaSD_wEloss(ThetaSD_noEloss): #theta_g distribution when E-loss and q/g is included
    def __init__(self):
        super().__init__()

    def thetag_matched_qw_eloss(self,theta,zcut,mode='all'): #beware that this thing might not be normalised
        eloss_q = eloss.qw_eloss(theta,'q')
        eloss_g = eloss.qw_eloss(theta,'g')
        quark_xs = spectrum.fit(self.pt_jet,self.rap_jet,'q','Pb')[0]
        gluon_xs = spectrum.fit(self.pt_jet,self.rap_jet,'g','Pb')[0]
        if mode=='q': # only quarks
            return eloss_q*quark_xs*self.medium_veto(theta,zcut,'q') # this is for the q/g fraction studies
        elif mode=='g': # only gluons
            return eloss_g*gluon_xs*self.medium_veto(theta,zcut,'g')
        else: # quarks and gluons
            return eloss_q*quark_xs*self.medium_veto(theta,zcut,'q')+eloss_g*gluon_xs*self.medium_veto(theta,zcut,'g')
    
    def raa(self,zcut): #beware that this thing might not be normalised
        thetamin = 0.
        thetamax = self.R_jet
        numerator= quad(lambda theta: self.thetag_matched_qw_eloss(theta,zcut,'all'),thetamin,thetamax,epsrel=1e-4)[0]
        quark_xs_vacuum = spectrum.fit(self.pt_jet,self.rap_jet,'q','p')[0]
        gluon_xs_vacuum = spectrum.fit(self.pt_jet,self.rap_jet,'g','p')[0]
        return numerator/(quark_xs_vacuum+gluon_xs_vacuum) # the denominator corresponds to the normalisation of the 
                                                           # theta_g distribution in vacuum

    def normalisation(self,zcut):
        thetamin = 0
        thetamax = self.R_jet
        return quad(lambda theta: self.vacuum_veto(theta,zcut,'q'),thetamin,thetamax)[0]
