import numpy as np
from scipy.integrate import quad
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import math
from scipy import special
from matplotlib import gridspec
from basics import Definitions
from elements import *
from ktg import *

prueba = Definitions()
font = {'color':  'black',
    'weight': 'normal',
    'size': 18
    }
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Palatino']
plt.rcParams['xtick.labelsize'] = 14
plt.rcParams['ytick.labelsize'] = 14
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['legend.handlelength'] = 1
plt.rcParams['axes.labelpad'] = 15

# Grab all the stuff we need
ktgvalues = np.loadtxt('ktg_values.dat', delimiter=' ', unpack=True)
rapvalues = np.loadtxt('yvalues.dat', delimiter=' ', unpack=True)
dsigmadktg_pt120y0 = np.loadtxt('results_ktg/vac_R0.4_pt120.0_yjet0.15.dat', delimiter=' ', unpack=True) 
dsigmadktg_pt50y0 = np.loadtxt('results_ktg/vac_R0.4_pt50.0_yjet0.15.dat', delimiter=' ', unpack=True)
#ktg_inclusive = np.loadtxt('results_ktg/vac_R0.4_pt120.0_yjet0.15.dat', delimiter=' ', unpack=True)
#ktg_inclusive_q = np.loadtxt('results_ktg/vac_flavq_R0.4_pt120.0_yjet-1.0.dat', delimiter=' ', unpack=True)
#print(noeloss.Qs2zero)
#exit()
with PdfPages('ktg-vac.pdf') as pdf:
  #----------------------------------------------------------------
  fig = plt.figure(figsize=(5,3.8))
  gs = gridspec.GridSpec(2, 2, height_ratios=[2,1],width_ratios=[1,1.2])
  ax = plt.gca()

  # generic setup
  ax0 = plt.subplot(gs[0])
  ax0.grid(True, lw=0.5, ls=':', zorder=0)
  ax0.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
 # ax0.set_ylim(0,40)
  ax0.set_xscale('log')
 # ax0.set_ylim(0.01,100)
  ax0.set_ylabel(r'$\frac{1}{\sigma}\frac{d\sigma}{dk_{t}}$',fontsize=16)
  n = np.size(rapvalues)
  colors = plt.cm.turbo(np.linspace(0,1,n))

  ax1 = plt.subplot(gs[1], sharey = ax0)
  ax1.set_xscale('log')
  ax2 = plt.subplot(gs[2],sharex = ax0)
  plt.setp(ax0.get_xticklabels(), visible=False)
  plt.setp(ax1.get_xticklabels(), visible=False)
  plt.setp(ax1.get_yticklabels(), visible=False)
 # plt.setp(ax2.get_yticklabels(), visible=False)
  ax3 = plt.subplot(gs[3],sharex = ax1,sharey = ax2)
  plt.setp(ax3.get_yticklabels(), visible=False)
  #plt.setp(ax3.get_xticklabels(), visible=False)
  #ax1.set_xlabel(r'$k_{t}=z\theta/R$',fontsize=16)
  ax2.set_ylabel(r'Ratio to $y=0$',fontsize=14)
  ax1.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
  ax1.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  #ax1.set_ylim([0.5,1.5])
  #tks=[0.6,0.8,1.,1.2,1.4]
  #ax1.set_yticks(tks)

  for i in range(0, n):
      icolor = i;
      y = rapvalues[i]
      dsigmadktg = np.loadtxt('results_ktg/vac_R0.4_pt120.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True) 
    #  norm= np.loadtxt('results_ktg/norm_flavall_R0.4_pt120.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
    #  dsigmadktg_mie = np.loadtxt('results_ktg/med_R0.4_pt120.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
      ax0.plot(ktgvalues/prueba.R_jet, dsigmadktg,color=colors[icolor])
      ax2.plot(ktgvalues/prueba.R_jet, dsigmadktg/dsigmadktg_pt120y0,color=colors[icolor])

     # caca=ax0.scatter(ktgvalues/prueba.R_jet, dsigmadktg,c=np.linspace(min(rapvalues),max(rapvalues),np.size(ktgvalues)),s=0,marker='o', cmap='turbo')

  Rc = prueba.Qs/120
  #ax0.plot([Rc,Rc],[0,ax0.get_ylim()[1]], ls='--', lw=1, color='gray')
  ax2.set_ylim([0.6,3.5])
  tks=[0.5,1,1.5,2, 2.5,3]
  ax2.set_yticks(tks)
  ax2.plot([ax2.get_xlim()[0],ax2.get_xlim()[1]],[1,1], ls='--', lw=1, color='gray')
  #ax1.plot([Rc,Rc],[ax1.get_ylim()[0],ax1.get_ylim()[1]], ls='--', lw=1, color='gray')
  #ax1.text(Rc-0.02,-0.07,r'$\frac{Q_s}{p_t}$', ha='left', va='baseline', 
  #        bbox=dict(facecolor='none',edgecolor='none'), transform=ax.transAxes, fontsize=12)

 # ax0.text(0.34, 0.86, r'$\alpha_s=0.2$, $R=0.4$', ha='left', va='baseline', 
 #          transform=ax.transAxes, fontsize=12, color='gray')
  #ax0.text(0.27, 0.74, r'$\hat q=1.5$ GeV$^2$/fm, $L=4$ fm', ha='left', va='baseline', 
  #        bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=12)
    
  ax0.text(0.2, 0.94, r'Vacuum', ha='left', va='baseline', transform=ax.transAxes, fontsize=12,color='gray')
  ax0.text(0.2, 0.88, r'$p_t=120$ GeV', ha='left', va='baseline', transform=ax.transAxes, fontsize=12,color='gray')
  ax1.text(0.65, 0.88, r'$p_t=50$ GeV', ha='left', va='baseline', transform=ax.transAxes, fontsize=12,color='gray')
  ax1.text(0.57, 0.94, r'$\alpha_s=0.2,R=0.4$', ha='left', va='baseline', transform=ax.transAxes, fontsize=12,color='gray')


  #axlist = [ax0,ax1] 
  #plt.subplots_adjust(hspace=.0,wspace=.0)
  #cbar=plt.colorbar(caca,ax=axlist)
  #ax0.text(0.98, 0.55, r'$y_{\rm jet}$', ha='left', va='baseline', 
  #        bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=16, rotation=270) 
  #pdf.savefig(bbox_inches='tight')
  #plt.close()

  #fig = plt.figure(figsize=(5,3.8))
  #gs = gridspec.GridSpec(2, 1, height_ratios=[2,1])
  #ax = plt.gca()

  # generic setup
  #ax2 = plt.subplot(gs[2],sharey = ax0)
  ax2.grid(True, lw=0.5, ls=':', zorder=0)
  ax2.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
 # ax0.set_ylim(0,40)
 # ax0.set_xscale('log')
  #ax2.set_xscale('log')
  #ax3.set_xscale('log')
 # ax0.set_ylim(0.01,100)
  #ax2.set_ylabel(r'Ratio to $y=0$',fontsize=16)
  #ax2.set_xlabel(r'$k_t=z\theta/R$',fontsize=16)
  n = np.size(rapvalues)
  colors = plt.cm.turbo(np.linspace(0,1,n))

  #ax3 = plt.subplot(gs[3], sharex = ax2)
  ax2.set_xlabel(r'$k_t=z\theta/R$',fontsize=16)
  ax3.set_xlabel(r'$k_t=z\theta/R$',fontsize=16)
  #ax3.set_ylabel(r'Ratio to $y=0$',fontsize=14)
  ax3.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
  ax3.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  ax1.set_xlim([0.0001,1])
  ax0.set_xlim([0.0001,1])
  tks=[0.001, 0.01, 0.1,1]
  ax3.set_xticks(tks)
  ax2.set_xticks(tks)

  for i in range(0, n):
      icolor = i;
      y = rapvalues[i]
      dsigmadktg = np.loadtxt('results_ktg/vac_R0.4_pt50.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True) 
     # norm= np.loadtxt('results_ktg/norm_flavall_R0.4_pt50.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
     # dsigmadktg_mie = np.loadtxt('results_ktg/med_R0.4_pt50.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
      ax1.plot(ktgvalues/prueba.R_jet, dsigmadktg,color=colors[icolor])
      ax3.plot(ktgvalues/prueba.R_jet, dsigmadktg/dsigmadktg_pt50y0,color=colors[icolor])

      caca=ax1.scatter(ktgvalues/prueba.R_jet, dsigmadktg,c=np.linspace(min(rapvalues),max(rapvalues),np.size(ktgvalues)),s=0,marker='o', cmap='turbo')

  Rc = prueba.Qs/120
  #ax0.plot([Rc,Rc],[0,ax0.get_ylim()[1]], ls='--', lw=1, color='gray')
  #ax1.set_xlim([0,1])
  #tks=[0.1,0.3,0.5,0.7,0.9]
  #ax1.set_xticks(tks)
  ax3.plot([ax3.get_xlim()[0],ax3.get_xlim()[1]],[1,1], ls='--', lw=1, color='gray')
  #ax1.plot([Rc,Rc],[ax1.get_ylim()[0],ax1.get_ylim()[1]], ls='--', lw=1, color='gray')
  #ax1.text(Rc-0.02,-0.07,r'$\frac{Q_s}{p_t}$', ha='left', va='baseline', 
  #        bbox=dict(facecolor='none',edgecolor='none'), transform=ax.transAxes, fontsize=12)

  #ax3.text(0.27, 0.94, r'$p_t=50$ GeV, $\alpha_s=0.2$, $R=0.4$', ha='left', va='baseline', 
  #        bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=12)
 # ax0.text(0.27, 0.74, r'$\hat q=1.5$ GeV$^2$/fm, $L=4$ fm', ha='left', va='baseline', 
 #         bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=12)
    
  #ax0.text(0.27, 0.84, r'Vacuum', ha='left', va='baseline', 
  #        bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=12)
  #plt.setp(ax0.get_xticklabels(), visible=False)

  axlist = [ax1,ax3] 
  plt.subplots_adjust(hspace=.0,wspace=.0)
  cbar=plt.colorbar(caca,ax=axlist)
  ax1.text(1.02, 0.5, r'$y_{\rm jet}$', ha='left', va='baseline', 
          bbox=dict(facecolor='white', edgecolor='none'), transform=ax.transAxes, fontsize=16, rotation=270) 
  pdf.savefig(bbox_inches='tight')
  plt.close()