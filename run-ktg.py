import sys
from basics import Definitions
from elements import *
from ktg import * 
import numpy as np

prueba = kTDyG_wEloss()
#flav = sys.argv[7]
#ktg = float(sys.argv[6])
yjet = float(sys.argv[3])
pt = float(sys.argv[2])
Rjet = sys.argv[1]
Lval = float(sys.argv[4])
a = float(sys.argv[5])
dsigmadkt = prueba.normalisation(a)
data = np.array([dsigmadkt])
data = data.T 
#print(ktg, dsigmadkt)
np.savetxt('paper_results/norm_med_all_R'+str(Rjet)+'_pt'+str(pt)+'_yjet'+str(yjet)+'.dat',data,fmt='%g')
#ktg_string = "{:.8f}".format(ktg)
#np.savetxt('paper_results/med_R'+str(Rjet)+'_pt'+str(pt)+'_yjet'+str(yjet)+'_ktg'+ktg_string+'.dat',data,fmt='%g')
