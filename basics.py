########################################
## QCD constants and running coupling ##
########################################

import numpy as np
import scipy as sp
import math
import sys
import scipy.special as special
from random import seed
from random import gauss
from random import random

#print('The length of the medium is:', sys.argv[1])
class Definitions:

    def __init__(self):
    # Jet and DyG definition
        self.R_jet = float(sys.argv[1])
        self.pt_jet = float(sys.argv[2]) # We make it an argument in GeV
        self.rap_jet = float(sys.argv[3])
        self.Q = self.pt_jet*self.R_jet

    # QCD parameters
        self.Cf = 4./3.
        self.Ca = 3.
        self.alphas_vac = 0.2 #alphas for VLEs
        self.ktcut = 1 #freezing for the coupling

    # Medium-related parameters
        self.alphas_med = 0.24 # alphas for MIEs
        self.GeVtoFm = 0.197
        self.wBH = 0.45 # Bethe Heitler frequency in GeV
        self.qhat0 = 1.5*self.GeVtoFm # GeV^3 medium qhat
        self.xi = float(sys.argv[4])/self.GeVtoFm # path length  
        self.wc = 0.5*self.qhat0*pow(self.xi,2)
        self.theta_c = 2./np.sqrt(self.qhat0*pow(self.xi,3))
        self.alphas_mz = 0.1265
        self.mz = 91.1876
        self.b0 = (33 - 10)/12./np.pi
        self.omega_c = 0.5*self.qhat0*pow(self.xi,2)
        self.Qs = np.sqrt(self.qhat0*self.xi) 

    # Other functions that we will need
    def step(self,x):
        return 1 * (x >= 0)

    def incomplete_gamma(self,x): # For a = 0-> Gamma(0,x) = \int_x^infty  dt e^-t/t See https://fr.wikipedia.org/wiki/Fonction_gamma_incomplete
        return special.exp1(x)
    
    def veto_region(self,z,theta):
        return self.step(theta-self.theta_c)*self.step(z*self.pt_jet*pow(theta,2)*self.xi-2.)*self.step(2*self.qhat0-pow(z*self.pt_jet,3)*pow(theta,4))
    
    
    def alphas_ref(self,scale):
            if scale>self.ktcut :
                return self.alphas_mz/(1.+2.*self.b0*self.alphas_mz*np.log(scale/self.mz))
            else:
                return self.alphas_mz/(1.+2.*self.b0*self.alphas_mz*np.log(self.ktcut/self.mz))

