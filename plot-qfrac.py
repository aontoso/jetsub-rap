import numpy as np
from scipy.integrate import quad
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import math
from scipy import special
from matplotlib import gridspec
from basics import Definitions
from elements import *
from ktg import *

prueba = Definitions()
font = {'color':  'black',
    'weight': 'normal',
    'size': 18
    }
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Palatino']
plt.rcParams['xtick.labelsize'] = 10
plt.rcParams['ytick.labelsize'] = 10
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['legend.handlelength'] = 1
#plt.rcParams['axes.labelpad'] = 15

# Grab all the stuff we need
ktgvalues = np.loadtxt('ktg_values.dat', delimiter=' ', unpack=True)
rapvalues = np.loadtxt('yvalues.dat', delimiter=' ', unpack=True)

with PdfPages('qg-fractions-30GeV.pdf') as pdf:
  #----------------------------------------------------------------
  fig = plt.figure(figsize=(5,3.8))
  gs = gridspec.GridSpec(2, 1, height_ratios=[2,1])
  ax = plt.gca()

  # generic setup
  ax0 = plt.subplot(gs[0])
  ax0.grid(True, lw=0.5, ls=':', zorder=0)
  ax0.tick_params(axis='both', which='both', direction='in', bottom=True, top=True, left=True, right=True )
  ax0.set_ylabel(r'$q$-fraction',fontsize=11)
  n = np.size(rapvalues)-1
  colors = plt.cm.turbo(np.linspace(0,1,n))

  ax1 = plt.subplot(gs[1], sharex = ax0)
  ax1.set_xlabel(r'$y_{\rm jet}$',fontsize=12)
  ax1.set_ylabel(r'Ratio to $pp$',fontsize=11)
  ax1.grid(True, lw=0.5, ls=':', zorder=0,color='silver')
  ax1.tick_params(axis='both', which='both', direction='in', bottom=True, top=True,left=True, right=True )
  ax1.set_ylim([0.8,2])
  tks=[1,1.2,1.4,1.6,1.8]
  ax1.set_yticks(tks)

  for i in range(0, n):
      icolor = i;
      y = rapvalues[i]
      all_vac = np.loadtxt('paper_results/norm_vac_all_R0.4_pt30.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True) 
      q_vac = np.loadtxt('paper_results/norm_vac_q_R0.4_pt30.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
      all_felix = np.loadtxt('paper_results/norm_felix_all_R0.4_pt30.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True) 
      q_felix = np.loadtxt('paper_results/norm_felix_q_R0.4_pt30.0_yjet'+str(y)+'.dat', delimiter=' ', unpack=True)
      qfrac_vac = q_vac/all_vac
      qfrac_felix = q_felix/all_felix
      
      ax0.plot(y, qfrac_felix,color=colors[icolor],marker='o')
      ax1.plot(y, qfrac_felix/qfrac_vac,color=colors[icolor],marker='o')
      


  ax1.plot([ax1.get_xlim()[0],ax1.get_xlim()[1]],[1,1], ls='--', lw=1, color='gray')


  ax0.text(0.05, 0.9, r'Pb+Pb, $\sqrt{s}=5.02$ TeV', ha='left', va='baseline', transform=ax.transAxes, fontsize=11,color='gray')
  ax0.text(0.05, 0.8, r'$R=0.4$, $p_t=30$~GeV', ha='left', va='baseline', transform=ax.transAxes, fontsize=11, color='gray')

  plt.setp(ax0.get_xticklabels(), visible=False)

  axlist = [ax0,ax1] 
  plt.subplots_adjust(hspace=.0,wspace=.0)
  pdf.savefig(bbox_inches='tight')
  plt.minorticks_on()

  plt.close()

  