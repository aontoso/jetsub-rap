########################################
###   Vacuum/In-medium DyG elements  ###
########################################

import numpy as np
import scipy as sp
import math
import matplotlib.pyplot as plt
import sys
import scipy.special as special
from scipy.integrate import quad
from basics import Definitions

class BranchKernel(Definitions):
    def __init__(self):
        super().__init__()

# Splitting function at DLA
    def Pvac(self,theta,z,fl='q'):
        if fl=='q':
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca
        alphas_vac = self.alphas_ref(self.pt_jet*self.R_jet)
        return 2.0*alphas_vac*Ci/(theta*z*math.pi)

# Splitting function a la BDMPS
    def Pmed(self,z,fl='q'):
        if fl=='q':
            Ci=self.Cf
        if fl=='g':
            Ci=self.Ca
        alpha_bar = self.alphas_med*Ci/math.pi
        return alpha_bar*np.sqrt(2*self.wc/self.pt_jet)*pow(z,-3./2.)

# Broadening function a la BDMPS
    def Pbroad(self,z,theta):
        return 2*theta*pow(z*self.pt_jet/self.Qs,2)*self.incomplete_gamma(pow(z*self.pt_jet*theta/self.Qs,2)) 

class Sudakov(BranchKernel):
    def __init__(self):
        super().__init__()

    def DeltaVac(self,kappa,a,fl='q'):
        if fl=='q':  
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca;
        alphas_vac = self.alphas_ref(self.pt_jet*self.R_jet)
        alpha_bar = alphas_vac*Ci/math.pi 
        #Area full: z*theta > kappa
        Afull = alpha_bar*pow(np.log(self.R_jet/kappa),2)/a
        return np.exp(-Afull) 
        
    def DeltaVac_int_veto(self,theta,kappa,a,fl='q'):
        if fl=='q':  
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca;
        zmin = max(kappa/pow(theta,a), 2./(pow(theta,2)*self.pt_jet*self.xi))    
        zmax = min(1.0,pow(2.*self.qhat0/(pow(self.pt_jet,3)*pow(theta,4)),1./3.))
        alphas_vac = self.alphas_ref(self.pt_jet*self.R_jet)
        alpha_bar = alphas_vac*Ci/math.pi 
        if zmax>zmin:
            return 2*alpha_bar*np.log(zmax/zmin)/theta
        else:
            return 0.0
     
    def DeltaVac_veto(self,kappa,a,fl='q'):  # Sudakov for vacuum like emissions 
        if fl=='q':  
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca;
        alphas_vac = self.alphas_ref(self.pt_jet*self.R_jet)
        alpha_bar = alphas_vac*Ci/math.pi 
        #Area full: z*theta > kappa. All vacuum like emissions
        Afull = alpha_bar*pow(np.log(self.R_jet/kappa),2)/a
        #Area veto: td < tf < L. Vacuum like emissions inside the veto region
        thetamin = max(pow(kappa,1./a),np.sqrt(2./(self.pt_jet*self.xi)),\
        self.theta_c,pow(2.*self.qhat0/pow(kappa*self.pt_jet,3),1./(4.-3.*a)) if a>4./3. else 0)
        thetamax = min(self.R_jet,\
        pow(2.*self.qhat0/pow(kappa*self.pt_jet,3),1./(4.-3.*a)) if a<=4./3. else self.R_jet)
        Aveto = 0
        if thetamin < thetamax:
            Aveto = quad(self.DeltaVac_int_veto,thetamin,thetamax,args=(kappa,a,fl))[0]
        return np.exp(-(Afull-Aveto))

    def DeltaMed_int(self,z,kappa,a,fl='q'):
        if fl=='q':  
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca
        Qs2 = pow(self.Qs,2)
        X2 = pow(z*self.pt_jet*self.R_jet,2)/Qs2
        tmin2 = pow(kappa/z,2./a)
        return self.Pmed(z,fl)*(X2*self.incomplete_gamma(X2)-np.exp(-X2)+np.exp(-tmin2*X2/pow(self.R_jet,2))-X2*tmin2*self.incomplete_gamma(X2*tmin2/pow(self.R_jet,2))/pow(self.R_jet,2))
        
    def DeltaMed(self,kappa,a,fl='q'): # Cross-checked with Adam's code
        if kappa > self.wc*self.R_jet/self.pt_jet:
            return 1
        else:
            zmin = kappa/pow(self.R_jet,a)
            zmax = np.minimum(self.wc/self.pt_jet,1.0)
            arg = quad(self.DeltaMed_int,zmin,zmax,args=(kappa,a,fl))[0]  
            return np.exp(-arg)  

class Spectrum(Definitions): 
    def __init__(self):
       super().__init__()

    def fit(self,pt,y,fl='q',A='p'): # We return the spectrum A (pt0 / x)^(n+beta Log[x/pt0]+gamma Log[x/pt0]^2)
                                     # and the exponent itself for different flavors and nuclear species

        absy = np.abs(y)
        if y==-1: rap = 'etabin0'
        elif 0 <= absy < 0.3: rap = 'etabin1'
        elif 0.3 <= absy < 0.8: rap = 'etabin2'
        elif 0.8 <= absy < 1.2: rap = 'etabin3'
        elif 1.2 <= absy < 1.6: rap = 'etabin4'
        elif 1.6 <= absy < 2.1: rap = 'etabin5'
        elif 2.1 <= absy < 2.8: rap = 'etabin6'
        elif 2.8 <= absy < 4.0: rap = 'etabin7'
        elif 4.0 <= absy < 5.0: rap = 'etabin8'
        else:
            print('You are outside the rapidity range')
            quit()

        if A=='p': pdf='vac'
        elif A=='Pb': pdf='med'
        else:
            print('Your colliding species is not valid')
            quit()
            
        if fl=='q': flav='quark'
        elif fl=='g': flav='gluon'
        else:
            print('your flavour is not valid')

        fname='prelim_eta_spectra_R4/eta_params_lhc/'+pdf+'_'+flav+'_R4_'+rap+'.dat'
        coeffs=np.loadtxt(fname)[:]
        return [coeffs[0]*pow(coeffs[1]/pt, coeffs[2]+coeffs[3]*np.log(pt/coeffs[1])+coeffs[4]*np.log(pt/coeffs[1])**2),coeffs[2]+coeffs[3]*np.log(pt/coeffs[1])+coeffs[4]*np.log(pt/coeffs[1])**2]

class Eloss(Spectrum): 
    def __init__(self):
        super().__init__()

    # Quenching weights
    def Q0(self, fl='q'):
        if fl=='q':  
            Ci = self.Cf
        if fl=='g':
            Ci = self.Ca
        omega_max = min(self.Qs/self.R_jet,self.omega_c)
        n = self.fit(self.pt_jet,self.rap_jet,fl,'Pb')[1]
        nu = n/self.pt_jet
        prefactor = 2.*self.alphas_med*Ci*np.sqrt(2.*self.omega_c/omega_max)/math.pi
        arg = 1.0-np.sqrt(math.pi*nu*omega_max)*special.erf(np.sqrt(nu*omega_max))-np.exp(-nu*omega_max)
        return np.exp(prefactor*arg)
    
    def qw_eloss_rough(self,theta,fl='q'):
        return self.Q0(fl)*self.step(self.theta_c-theta)+self.step(theta-self.theta_c)*self.Q0(fl)*self.Q0('g')

    def qw_eloss_coherent(self,fl='q'):
        return self.Q0(fl)
   